# Projeto Sistema de Delivery

> A atividade consiste em fazer o levantamento de requisitos a partir das estórias do usuário e realizar a análise, considerando os tópicos abordados em sala e as leituras recomendadas.

### Questões (issues)

1. [Que tipo de problema essa solução resolve?](https://gitlab.com/CaioMeneses/Projeto-Fundamentos/-/issues/5)
1. [Quais os requisitos foram levantados?](https://gitlab.com/CaioMeneses/Projeto-Fundamentos/-/issues/6)
1. [Descreva as etapas do desenvolvimento do seu sistema usando o modelo em cascata.](https://gitlab.com/CaioMeneses/Projeto-Fundamentos/-/issues/8)
1. [Descreva as etapas do desenvolvimento dividindo em Sprints.](https://gitlab.com/CaioMeneses/Projeto-Fundamentos/-/issues/9)
1. [Criar o diagrama de fluxo de dados.](https://gitlab.com/CaioMeneses/Projeto-Fundamentos/-/issues/10)
1. [Crie o diagrama de entidade relacionamento.](https://gitlab.com/CaioMeneses/Projeto-Fundamentos/-/issues/11)
1. [Escreva um relatório em markdown no repositório do seu projeto fazendo um comparativo sobre sobre o desenvolvimento de software utilizando metodologia tradicional e usando metodologia ágil.](https://gitlab.com/CaioMeneses/Projeto-Fundamentos/-/issues/15)
1. [Usando a notação UML, com relação ao seu projeto, crie: O diagrama de classe; Diagrama de implantação.](https://gitlab.com/CaioMeneses/Projeto-Fundamentos/-/issues/12)
1. [Crie um dicionário de dados para sua aplicação.](https://gitlab.com/CaioMeneses/Projeto-Fundamentos/-/issues/13)
1. [Com base no artigo "Perspectivas do valor da tecnologia da informação", extraía os principais conceitos e contextualize aplicando ao seu projeto.](https://gitlab.com/CaioMeneses/Projeto-Fundamentos/-/issues/14)