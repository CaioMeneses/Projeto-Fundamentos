# Desenvolvimento de Software: Metodologia Tradicional x Metodologia Ágil

> É fundamental repensar os modelos de negócios e adotar abordagens mais ágeis e flexíveis, capazes de acompanhar as mudanças do mercado.

Com essa afirmação, logo podemos discutir sobre as duas metodologias mais conhecidas no mercado atualmente:
- [Metodologia Tradicional](#metodologia-tradicional)
- [Metodologia Ágil](#metodologia-ágil)

## Contexto Histórico

As *metodologias de criação de software* surgiram como uma resposta aos desafios enfrentados no desenvolvimento de software ao longo da história da computação.

O contexto histórico destaca a evolução das metodologias de desenvolvimento de software em resposta às crescentes complexidades dos projetos de software e à necessidade de abordagens mais eficazes e eficientes. As metodologias continuam a se adaptar às mudanças tecnológicas e às demandas do mercado, buscando melhorar a qualidade, a colaboração e a entrega de software.

### Metodologia Tradicional

> A metodologia tradicional é um método de desenvolvimento de projetos que segue uma abordagem linear de planejamento bem estruturada, com passo a passo, prazo, orçamento, execução e entrega.

Dessa forma, não possui muita *flexibilidade* em relação às mudanças. É um modelo considerado sequencial, ou seja, a próxima etapa só deve ser executada quando a anterior for concluída, não é possível adiantar uma tarefa. O atraso só é possível de acontecer quando outras tarefas atrasam também. 

Nesse método, deve ser executado exatamente o que foi planejado, com foco total no resultado final e na entrega do prazo, tudo dentro do orçamento e com a qualidade prometida, sem considerar mudanças no comportamento do usuário ou identificação de pontos de melhoria. É definido o valor quando o projeto estiver completamente concluído.

![](https://dkrn4sk0rn31v.cloudfront.net/2019/03/22141228/imagem-metodologia-scrum.png)

### Metodologia Ágil 

> Esse método tem como objetivo satisfazer o cliente diante da entrega contínua e adiantada do software com o valor definido.

A palavra ágil, nesse caso, não significa agilidade, mas promove a ideia de poder dividir o projeto em partes menores, conseguir controlar e gerenciar as mudanças que aparecem durante o decorrer do projeto e ter foco principal na entrega do valor ao cliente.

Além disso, defende um desenvolvimento sustentável juntamente com excelentes técnicas e designs, aumentando a agilidade. As melhores arquiteturas têm um valor maior do que as equipes auto-organizáveis.

![](https://coodesh.com/blog/wp-content/uploads/2021/07/metodo-agile-1-scaled.jpg)

### Comparativos

A principal diferença entre as metodologias tradicionais e ágeis está na abordagem para lidar com a mudança e na ênfase na comunicação, colaboração e entrega incremental. As metodologias ágeis são mais adequadas para projetos com requisitos voláteis ou desconhecidos, enquanto as metodologias tradicionais ainda podem ser relevantes para projetos com requisitos bem definidos e pouca probabilidade de mudanças. A escolha da metodologia depende do contexto do projeto e das necessidades do cliente.